# Changelog

#### 1.0.2

- Chore: support base `3.0`.

#### 1.0.1

- Chore: allow `text` ^2.

## 1.0.0

- Initial version, import modules from clean platform v0.3.37 and destill all
  language modules.
