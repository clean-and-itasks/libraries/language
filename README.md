# language

This library provides some types and functionality for working with natural
language such as pluralisation.

Currently, only English is supported.

## Licence

The package is licensed under the BSD-2-Clause license; for details, see the
[LICENSE](/LICENSE) file.
